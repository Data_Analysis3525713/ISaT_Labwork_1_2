import json
import pandas as pd
import numpy as np
import time
import pprint as pp


from typing import List, Any, Type, Dict
from apriori_python import apriori as py_apr
from datetime import timedelta
from efficient_apriori import apriori as ef_apr
from fpgrowth_py import fpgrowth, constructTree, fpgrowthFromFile
from PyARMViz import datasets, PyARMViz, adjacency_graph_plotly, adjacency_parallel_category_plot
from PyARMViz.Rule import Rule as ARMRule

from affinity_analysis_test import *


def prepare_big_data_from_excel(path_to_excel:str, sheet_name:int) -> List[Any]:
    df = pd.read_excel(path_to_excel, sheet_name=sheet_name) # opening source file

    transactions = [] # defining new array to save transactions
    df = df.transpose() # transposing table sheet because we can read only by columns
    # df = df.replace(np.nan,'') # reading file and replacing empty cells with empty strings

    # for i in df.columns: 
        # temp = [x for x in df[i].tolist() if x !=''] # making array that contains row elements without empty strings
        # transactions.append(temp) # adding array of  to transactions database

    # printing transactions database by rows and numbers of these rows
    #for i,j in enumerate(transactions):
    #    print(f"{i}:{j}\n")

    #apriori_python calculations
    js = df.to_json(path_or_buf="./test2.json")
    return df

def read_json(path:str):
    with open(path,"r") as file:
        data = json.load(file)
    return data
    # for num,item, in enumerate(data.keys()):
    #     print(f"#{num} data:{data[item]}")
    #     if num == 10:
    #         break

def group_by_invoice():
    data = read_json("./test2.json")
    dict_records = {}

    for item in data.keys():
        is_exist = data[item]["InvoiceNo"] == dict_records.get(data[item]["InvoiceNo"],{}).get("InvoiceNo",-1)

        if is_exist:
            record = dict_records.get(data[item]["InvoiceNo"],{})
            record["StockCode"].append(str(data[item]["StockCode"]))
            record["Description"].append(data[item]["Description"])
            dict_records[data[item]["InvoiceNo"]] = record
        else:
            record = {
                    "InvoiceNo":data[item]["InvoiceNo"],
                    "StockCode":[str(data[item]["StockCode"])],
                    "Description":[data[item]["Description"]],
                }
            dict_records[data[item]["InvoiceNo"]] = record
    
    with open("group_by_invoice.json","w") as file:
        json.dump(dict_records,file,indent=2)
            


def get_transaction(path:str):
    data = read_json(path)

    transactions = []
    for transaction in data.keys():
        transactions.append(data[transaction]["StockCode"])
    
    return transactions[0:9]



if __name__ == "__main__":
    path = "./Online retail.xlsx"
    # t = prepare_big_data_from_excel(path,0)
    # print(t)
    # read_json("./test2.json")
    # group_by_invoice()
    transactions = get_transaction("./group_by_invoice.json")
    # print (f"transactions{type(transactions[0][0][0])}")
    transactions2 = prepare_small_data_from_excel("./Affinity_analysis.xlsx",2)
    # print (f"transactions2{type(transactions2[0][0][0])}")

    # small data
    apriori_python_calcs(transactions2,0.5,0.6)
    apriori_python_calcs(transactions2,0.5,0.8)

    effricient_apriori_calc(transactions2,0.5,0.6)
    effricient_apriori_calc(transactions2,0.5,0.8)
    
    fpgrowth_py_calculator(transactions2,0.5,0.6,True)
    fpgrowth_py_calculator(transactions2,0.5,0.8,False)

    # big data
    # apriori_python_calcs(transactions,0.5,0.6)
    # apriori_python_calcs(transactions,0.5,0.8)

    # effricient_apriori_calc(transactions,0.5,0.6)
    # effricient_apriori_calc(transactions,0.5,0.8)
    
    # fpgrowth_py_calculator(transactions,0.1,0.1,False)
    # fpgrowth_py_calculator(transactions,0.1,0.1,False)

    ## ------------------------
    #print(transactions)
    #apriori_python_calcs(transactions2,0.5,0.8)
    # fpgrowth_py_calculator(transactions2,0.5,0.6,True)
    # fpgrowth_py_calculator(transactions2,0.5,0.8,False)
