import time
import pandas as pd
import numpy as np
import pprint as pp

from typing import List, Any, Tuple, Type
from apriori_python import apriori as py_apr
from datetime import timedelta
from efficient_apriori import apriori as ef_apr
from fpgrowth_py import fpgrowth, constructTree, fpgrowthFromFile
from PyARMViz import datasets, PyARMViz, adjacency_graph_plotly, adjacency_parallel_category_plot
from PyARMViz.Rule import Rule as ARMRule


def prepare_small_data_from_excel(path_to_excel:str, sheet_name:int) -> List[Any]:
    df = pd.read_excel(path_to_excel, sheet_name=sheet_name) # opening source file

    transactions = [] # defining new array to save transactions
    df = df.transpose() # transposing table sheet because we can read only by columns
    df = df.replace(np.nan,'') # reading file and replacing empty cells with empty strings

    for i in df.columns: 
        temp = [x for x in df[i].tolist() if x !=''] # making array that contains row elements without empty strings
        transactions.append(temp) # adding array of  to transactions database

    # printing transactions database by rows and numbers of these rows
    #for i,j in enumerate(transactions):
    #    print(f"{i}:{j}\n")

    #apriori_python calculations
    return transactions


def apriori_python_calcs(transactions:List[List[Any]], support:float, min_confidence:float):
    print("\n----------Apriopri_python calculations----------\n")
    start_time = time.monotonic()
    py_apr_frequent_itemset, py_apr_rules = py_apr(transactions, minSup=support, minConf=min_confidence)
    end_time = time.monotonic()

    print(f"time for caclulate {timedelta(seconds=end_time-start_time)}")
    print("frequnt itemset:")
    pp.pprint(py_apr_frequent_itemset)
    print("\nRules:")
    for i in py_apr_rules:
        print(f"{i}")


# # efficient_apriori calculations
def effricient_apriori_calc(transactions:List[List[Any]], support:float, min_confidence:float):
    print("\n----------Efficient_apriori calculations----------\n")
    start_time = time.monotonic()
    ef_apr_frequent_itemset, ef_apr_rules = ef_apr(transactions, min_support=support, min_confidence=min_confidence)
    end_time = time.monotonic()

    print(f"time for caclulate {timedelta(seconds=end_time-start_time)}")
    print("frequnt itemset:")
    pp.pprint(ef_apr_frequent_itemset)
    print("\nRules:")
    for i in ef_apr_rules:
        print(f"{i}")


def fpgrowth_py_calculator(transactions:List[List[Any]], support:float, min_confidence:float, visual:bool):
# fpgrowth_py calculations
    print("\n----------FPGrowth_py calculations----------\n")

    start_time = time.monotonic()
    fpg_frequent_itemset, fpg_rules = fpgrowth(transactions, minSupRatio=support, minConf=min_confidence)

    # fpg_frequent_itemset, fpg_rules = fpgrowthFromFile('./Online Retail.xlsx', minSupRatio=0.5, minConf=0.6)
    end_time = time.monotonic()
    print(f"time for caclulate {timedelta(seconds=end_time-start_time)}")

    print("frequnt itemset:")
    print(fpg_frequent_itemset)
    # for i in fpg_frequent_itemset:
        # print(f"{i}")
    print("\nRules:")
    print(fpg_rules)
    # for i in fpg_rules:
        # print(f"{i}")
    
    if visual:
        _visualize_fpgrowth_data(transactions,fpg_rules)

def generate_fpg_rules(transactions:List[Any], support:float, min_confidence:float):
    fpg_frequent_itemset, fpg_rules = fpgrowth(transactions, minSupRatio=support, minConf=min_confidence)
    return fpg_rules
    

def _visualize_fpgrowth_data(transactions:List[Any], fpg_rules:Tuple[List[Any],List[Any]]):
    ARMRules = _compile_rules(transactions, fpg_rules)
    adjacency_parallel_category_plot(ARMRules)

def _compile_rules(transactions:List[Any], fpg_rules:Tuple[List[Any],List[Any]]) -> List[Type[ARMRule]]:
    ARMRules = []
    for rule in fpg_rules:
        ARMRules.append(ARMRule(
            lhs=[_rule for _rule in rule[0]],
            rhs=[_rule for _rule in rule[1]],
            count_full=len(rule[0])+len(rule[1]),
            count_lhs = len(rule[0]),
            count_rhs = len(rule[1]),
            num_transactions = len(transactions)
            ))
    return ARMRules

def generate_meta_data(transactions:List[Any], support:float, min_confidence:float):
    rules = generate_fpg_rules(transactions, support, min_confidence)
    compiled_rules = _compile_rules(transactions, rules)
    show_meta_data_by_rules(compiled_rules)


def show_meta_data_by_rules(rules:List[Type[ARMRule]]):
    for num,rule in enumerate(rules):
        support_lhs = rule.count_lhs/rule.num_transactions
        support_rhs = rule.count_rhs/rule.num_transactions
        importance = rule.support - support_lhs * support_rhs
        print(f"#{num}Rule:{rule}:\n\tSupport:{rule.support}\n\tConfidence{rule.confidence}\n\tImportance{importance}")

if __name__ == "__main__":
    path = "./Affinity_analysis.xlsx"
    sheet_name = 2
    transactions = prepare_small_data_from_excel(path,sheet_name)
    print(transactions)
    exit(-1)
    # apriori_python_calcs(transactions,0.5,0.4)
    apriori_python_calcs(transactions, 0.5, 0.6)

    # effricient_apriori_calc(transactions,0.5,0.4)
    # fpgrowth_py_calculator(transactions, 0.2, 0.4, True)
    # generate_meta_data(transactions, 0.01, 0.01)
    generate_meta_data(transactions, 0.5, 0.6)










# for i in mylist:
#     for j in rai:
#         print(j)
#         if j == '':
#             i.remove(j)
#             # mylist[mylist.index(i)].remove(j)
#             #del mylist[mylist.index(i)][mylist[mylist.index(i)].index(j)]

# # print(f"All:{df}")
# print(f"mylist:{mylist}")